# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.0.0

- major: Parameters names were changes to be more intuitive. Added support to trigger different pipelines definitions

## 0.3.0

- minor: Don't use pipe.yml to specify parameters schema

## 0.2.1

- patch: Patch version bump

## 0.2.0

- minor: Rename the pipe

## 0.1.0

- minor: Initial release

