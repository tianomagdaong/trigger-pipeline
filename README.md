# Bitbucket Pipelines pipe: trigger Bitbucket Pipelines pipeline

Trigger a Bitbucket Pipelines pipeline

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: atlassian/trigger-pipeline:1.0.0
    variables:
      APP_PASSWORD: '<string>'
      REPO: 'your-awesome-repo'
      # ACCOUNT: '<string>' # Optional
      # BRANCH_NAME: '<string>' # Optional
      # WAIT: '<boolean>' # Optional
      # WAIT_MAX_TIMEOUT: '<string>' # Optional
      # DEBUG: '<boolean>' # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| APP_PASSWORD (*)      | Bitbucket app password. See how to generate it [here](https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html). |
| REPO (*)              | Name of the repository. |
| ACCOUNT               | Bitbucket account. Default: current account available as `$BITBUCKET_REPO_OWNER`. |
| BRANCH_NAME        | The branch name. Default `master`. |
| PIPELINE_TYPE         | The type of Bitbucket Pipeline as described [here](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html#Configurebitbucket-pipelines.yml-Keyconcepts). Currently only `custom` and `branches` are allowed. If no PIPELINE_TYPE is set, the pipeline will be inferred from the `bitbucket-pipelines.yml` configuration in your `REPO`. `PIPELINE_PATTERN` is required when using this parameter. |
| PIPELINE_PATTERN      | The pattern to use when looking up the exact pipeline definition to run. Each pipeline type will have multiple pipeline definitions depending on the name of your branch, tag etc as described in the [Pipelines docs](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html).
| WAIT                  | Wait until the triggered build finishes. Default: `false`. Set this to `true` if you want to wait for the triggered build completion. If this build fails, the pipe fails as well. |
| WAIT_MAX_TIMEOUT      | Maximum time in seconds to wait until the triggered build finishes. Default: `3600`. Pipe will fail if the timeout is reached.| 
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Prerequisites

To use this pipe you will have to generate an App password. [This page](https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html) should help you to do this. Remember to check the `Pipelines Write` and `Repositories Read` permissions checkboxes when generating the app password.

## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/trigger-pipeline:1.0.0
    variables:
      APP_PASSWORD: $APP_PASSWORD
      REPO: 'your-awesome-repo'
```

Advanced example:

```yaml
script:
  - pipe: atlassian/trigger-pipeline:1.0.0
    variables:
      APP_PASSWORD: $APP_PASSWORD
      REPO: 'your-awesome-repo'
      BRANCH_NAME: develop
      WAIT: true
```

## Support
If you'd like help with this pipe, or you have an issue or feature request, [let us know on Community](https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes).

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.