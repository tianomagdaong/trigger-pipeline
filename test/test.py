import os

import pytest
from bitbucket_pipes_toolkit.test import PipeTestCase


class TriggerBuildTestCase(PipeTestCase):

    def test_build_successfully_started_master(self):
        result = self.run_container(environment={
            'ACCOUNT': os.getenv('ACCOUNT'),
            'APP_PASSWORD': os.getenv('APP_PASSWORD'),
            'REPO': os.getenv('REPO', 'test-trigger-build'),
            'WAIT': 'true'
        })

        self.assertRegexpMatches(
            result, r'✔ Build number \d+ for [-\w]+@master finished successfully')

    def test_build_successfully_started_default_account(self):
        result = self.run_container(environment={
            'BITBUCKET_REPO_OWNER': os.getenv('ACCOUNT'),
            'APP_PASSWORD': os.getenv('APP_PASSWORD'),
            'REPO': os.getenv('REPO', 'test-trigger-build'),
            'WAIT': 'true'
        })

        self.assertRegexpMatches(
            result, r'✔ Build number \d+ for [-\w]+@master finished successfully')


    @pytest.mark.skip(reason='Skip until we add tags support back')
    def test_build_successfull_for_tag(self):
        tagname = '1.0.0'
        result = self.run_container(environment={
            'ACCOUNT': os.getenv('ACCOUNT'),
            'APP_PASSWORD': os.getenv('APP_PASSWORD'),
            'REPO': os.getenv('REPO', 'test-trigger-build'),
            'WAIT': 'true',
            'REF_TYPE': 'tag',
            'BRANCH_NAME': tagname
        })

        self.assertRegexpMatches(
            result, rf'✔ Build number \d+ for [-\w]+@{tagname} finished successfully')

    @pytest.mark.skip(reason='Skip until we add tags support back')
    def test_build_fails_for_wrong_tag(self):
        tagname = 'no-such-tag'
        result = self.run_container(environment={
            'ACCOUNT': os.getenv('ACCOUNT'),
            'APP_PASSWORD': os.getenv('APP_PASSWORD'),
            'REPO': os.getenv('REPO', 'test-trigger-build'),
            'WAIT': 'true',
            'REF_TYPE': 'tag',
            'BRANCH_NAME': tagname
        })

        self.assertRegexpMatches(
            result, rf'✖ Failed to get the tag info for {tagname}')

    def test_build_should_fail_if_downstream_fails(self):
        ref = 'always-fail'
        result = self.run_container(environment={
            'ACCOUNT': os.getenv('ACCOUNT'),
            'APP_PASSWORD': os.getenv('APP_PASSWORD'),
            'REPO': os.getenv('REPO', 'test-trigger-build'),
            'BRANCH_NAME': ref,
            'WAIT': 'true'
        })

        self.assertRegexpMatches(
            result, rf'✖ Build number \d+ for [-\w]+@{ref} failed')

    def test_pipe_should_timeout(self):
        ref = 'always-fail'
        result = self.run_container(environment={
            'ACCOUNT': os.getenv('ACCOUNT'),
            'APP_PASSWORD': os.getenv('APP_PASSWORD'),
            'REPO': os.getenv('REPO', 'test-trigger-build'),
            'BRANCH_NAME': ref,
            'WAIT': 'true',
            'WAIT_MAX_TIME': 1
        })

        self.assertRegexpMatches(
            result, '✖ Timeout waiting for the pipeline completion')


class InvalidParametersTestCase(PipeTestCase):

    def test_no_parameters(self):
        result = self.run_container()
        self.assertIn('APP_PASSWORD:\n- required field', result)

    def test_no_repo(self):
        result = self.run_container()
        self.assertIn('REPO:\n- required field', result)

    def test_pipe_fails_for_nonexistent_branch(self):
        result = self.run_container(environment={
            'ACCOUNT': os.getenv('ACCOUNT'),
            'APP_PASSWORD': os.getenv('APP_PASSWORD'),
            'REPO': os.getenv('REPO', 'test-trigger-build'),
            'BRANCH_NAME': 'no-such-branch',
        })

        self.assertRegexpMatches(
            result, r"✖ Account, repository or branch doesn't exist")

    def test_pipe_wait_should_be_boolean(self):
        result = self.run_container(environment={
            'WAIT': 'not-a-bool',
        })

        self.assertRegexpMatches(
            result, r"WAIT:\n- must be of boolean type")

    def test_pipe_should_fail_wrong_account(self):
        result = self.run_container(environment={
            'ACCOUNT': 'NoSuchAccount',
            'APP_PASSWORD': os.getenv('APP_PASSWORD'),
            'REPO': os.getenv('REPO', 'test-trigger-build'),
            'BRANCH_NAME': 'no-such-branch',
        })

        self.assertRegexpMatches(
            result, r"✖ API request failed with status 401")

    def test_pipe_should_fail_wrong_repo(self):
        result = self.run_container(environment={
            'ACCOUNT': os.getenv('ACCOUNT'),
            'APP_PASSWORD': os.getenv('APP_PASSWORD'),
            'REPO': 'no-such-repo-hopefully',
            'BRANCH_NAME': 'no-such-branch',
        })

        self.assertRegexpMatches(
            result, r"✖ Account, repository or branch doesn't exist")



class PipelineTypeTestCase(PipeTestCase):

    def test_no_pattern_should_fail_validation(self):
        result = self.run_container(environment={
            'ACCOUNT': os.getenv('ACCOUNT'),
            'APP_PASSWORD': os.getenv('APP_PASSWORD'),
            'REPO': 'no-such-repo-hopefully',
            'BRANCH_NAME': 'master',
            'PIPELINE_TYPE': 'custom'
        })

        self.assertRegexpMatches(
            result, r"field 'PIPELINE_PATTERN' is required")

    def test_custom_with_pattern(self):
        result = self.run_container(environment={
            'ACCOUNT': os.getenv('ACCOUNT'),
            'APP_PASSWORD': os.getenv('APP_PASSWORD'),
            'REPO': 'test-trigger-build',
            'BRANCH_NAME': 'custom-pipeline',
            'PIPELINE_TYPE': 'custom',
            'PIPELINE_PATTERN': 'test-custom',
            'WAIT': 'true'
        })

        self.assertRegexpMatches(
            result, r"Build number \d+ for [-\w]+@custom-pipeline finished successfully")


    def test_branches_with_pattern(self):
        result = self.run_container(environment={
            'ACCOUNT': os.getenv('ACCOUNT'),
            'APP_PASSWORD': os.getenv('APP_PASSWORD'),
            'REPO': 'test-trigger-build',
            'BRANCH_NAME': 'test-branches-with-pattern',
            'PIPELINE_TYPE': 'branches',
            'PIPELINE_PATTERN': 'test-branches-with-pattern',
            'WAIT': 'true'
        })

        self.assertRegexpMatches(
            result, r"Build number \d+ for [-\w]+@test-branches-with-pattern finished successfully")
